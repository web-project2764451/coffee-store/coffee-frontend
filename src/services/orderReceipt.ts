import type { OrderReceipt } from '@/types/OrderReceipt'
import http from './http'
import type { OrderReceiptItem } from '@/types/OrderReceiptItem'

type StockReceiptDto = {
  stockReceiptItems: {
    stockId: number
    qty: number
    priceUnit: number
  }[]
  userId: number
}

function getOrderReceipt(id: number) {
  return http.get(`/stock-receipt/${id}`)
}

function getOrderReceipts() {
  return http.get('/stock-receipt')
}

function addOrderReceipt(orderReceipt: OrderReceipt, orderReceiptItems: OrderReceiptItem[]) {
  const StockReceiptDto: StockReceiptDto = {
    stockReceiptItems: [],
    userId: 0
  }
  StockReceiptDto.userId = orderReceipt.employeeId
  StockReceiptDto.stockReceiptItems = orderReceiptItems.map((item) => {
    return {
      stockId: item.stockId,
      qty: item.qty,
      priceUnit: item.price
    }
  })
  return http.post('/stock-receipt', StockReceiptDto)
}
export default { addOrderReceipt, getOrderReceipt, getOrderReceipts }
