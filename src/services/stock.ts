import type { Stock } from '@/types/Stock'
import http from './http'

function addStock(stock: Stock) {
  return http.post('/stocks', stock)
}

function updateStock(stock: Stock) {
  return http.patch(`/stocks/${stock.id}`, stock)
}

function delStock(stock: Stock) {
  return http.delete(`/stocks/${stock.id}`)
}

function getStock(id: number) {
  return http.get(`/stocks/${id}`)
}

function getStocks() {
  return http.get('/stocks')
}

export default { addStock, updateStock, delStock, getStock, getStocks }
