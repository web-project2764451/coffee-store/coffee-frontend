import type { Expenses } from '@/types/Expenses'
import http from './http'

function addExpenses(expenses: Expenses) {
  return http.post('/expensess', expenses)
}

function updateExpenses(expenses: Expenses) {
  return http.patch(`/expensess/${expenses.id}`, expenses)
}

function delExpenses(expenses: Expenses) {
  return http.delete(`/expensess/${expenses.id}`)
}

function getExpenses(id: number) {
  return http.get(`/expensess/${id}`)
}

function getExpensess() {
  return http.get('/expensess')
}

export default { addExpenses, updateExpenses, delExpenses, getExpenses, getExpensess }
