import type { CheckStock } from '@/types/CheckStock'
import http from './http'
import type { CheckStockItem } from '@/types/CheckStockItem'

type CheckStockDto = {
  checkStockItems: {
    stockId: number
    remain: number
  }[]
  userId: number
}

function getCheckStock(id: number) {
  return http.get(`/check-stock/${id}`)
}

function getCheckStocks() {
  return http.get('/check-stock')
}

function addCheckStock(orderReceipt: CheckStock, checkStockItems: CheckStockItem[]) {
  const CheckStockDto: CheckStockDto = {
    checkStockItems: [],
    userId: 0
  }
  CheckStockDto.userId = orderReceipt.employeeId
  CheckStockDto.checkStockItems = checkStockItems.map((item) => {
    return {
      stockId: item.stockId,
      remain: item.remain
    }
  })
  return http.post('/check-stock', CheckStockDto)
}
export default { addCheckStock, getCheckStock, getCheckStocks }
