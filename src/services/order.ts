import type { Receipt } from '@/types/Receipt'
import http from './http'
import type { ReceiptItem } from '@/types/ReceiptItem'

type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
    sweet: number
    category: string
  }[]
  memberId: number
  userId: number
  memberDiscount: number
  change: number
  paymentType: string
  amount: number
  total: number
  totalBefore: number
}

function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  console.log(receipt)
  console.log(receiptItems)
  const receiptDto: ReceiptDto = {
    orderItems: [],
    userId: 0,
    memberId: 0,
    memberDiscount: 0,
    change: 0,
    paymentType: '',
    amount: 0,
    total: 0,
    totalBefore: 0
  }
  receiptDto.memberId = receipt.memberId!
  receiptDto.userId = receipt.userId
  receiptDto.memberDiscount = receipt.memberDiscount
  receiptDto.change = receipt.change
  receiptDto.paymentType = receipt.paymentType
  receiptDto.amount = receipt.amount
  receiptDto.total = receipt.total
  receiptDto.totalBefore = receipt.totalBefore || 0
  receiptDto.orderItems = (receiptItems as ReceiptItem[]).map((item) => {
    return {
      productId: item.productId as number,
      qty: item.qty,
      sweet: item.sweet,
      category: item.category
    }
  })
  console.log(receiptDto)
  receiptDto.orderItems = receiptDto.orderItems.filter((item) => item.productId !== undefined)
  return http.post('/orders', receiptDto)
}
function getOrders() {
  return http.get('/orders')
}
function getOrder(id: number) {
  return http.get(`/orders/${id}`)
}

export default { addOrder, getOrders, getOrder }
