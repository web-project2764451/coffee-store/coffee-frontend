import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: () => import('../views/HomeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/UsersView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/products',
      name: 'products',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/ProductView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pos',
      name: 'pos',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/pos/PosView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/member',
      name: 'member',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/MemberVeiw.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/stock',
      name: 'stock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/stock/StockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/stock-receipt',
      name: 'stockreceipt',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/stock/StockReceiptView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/check-stock',
      name: 'checkstock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/stock/CheckStockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/promotion',
      name: 'promotion',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/PromotionView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/checktime',
      name: 'checktime',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CheckTimeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/receiptHistory',
      name: 'receiptHistory',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/pos/ReceiptHistoryView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/salary',
      name: 'salary',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/SalaryView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/branchs',
      name: 'branchs',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/BranchView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/expensess',
      name: 'expensess',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/ExpensesView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    }
  ]
})
function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}
router.beforeEach((to, from) => {
  if (to.meta.requireAuth && !isLogin()) {
    router.replace('/login')
  }
})
export default router
