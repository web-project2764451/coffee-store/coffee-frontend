import type { Role } from './Role'
type Gender = 'male' | 'female' | 'others'
type User = {
  id?: number
  email: string
  password: string
  fullName: string
  image: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
}
function getImageUrl(user: User) {
  return `http://localhost:3000/images/users/${user.image}`
}
export type { Gender, User, getImageUrl }
