type Expenses = {
    id?: number
    branchId: number
    branchName: string
    billType: string
    totalAmount: number
}
export { type Expenses}