type Stock = {
  id?: number
  name: string
  qty: number
  priceUnit: number
  value: number
}
export { type Stock }
