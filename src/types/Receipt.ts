import type { Member } from './Member'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id: number
  created: Date
  totalBefore?: number
  total: number
  memberDiscount: number
  qty: number
  amount: number
  change: number
  paymentType: string
  user?: User
  userId: number
  member?: Member
  memberId?: number
  receiptItems?: ReceiptItem[]
}

export { type Receipt }
