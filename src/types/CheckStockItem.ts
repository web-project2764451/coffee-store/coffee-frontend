import type { Stock } from './Stock'

type CheckStockItem = {
  id: number
  name: string
  lastQty: number
  use: number
  remain: number
  stockId: number
  stock?: Stock
}

export { type CheckStockItem }
