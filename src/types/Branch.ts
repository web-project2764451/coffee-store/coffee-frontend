type Branch = {
    id?: number
    branchName: string
    address: string
}

export type { Branch }