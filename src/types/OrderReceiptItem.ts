import type { Stock } from './Stock'

type OrderReceiptItem = {
  id: number
  name: string
  price: number
  qty: number
  total: number
  stockId: number
  stock?: Stock
}

export type { OrderReceiptItem }
