import type { Member } from './Member'
import type { Product } from './Product'
import type { Type } from './Type'

const defaultReceiptItem = {
  id: -1,
  name: '',
  price: 0,
  unit: 0,
  productId: -1,
  product: null
}
type ReceiptItem = {
  id?: number
  name: string
  price: number
  qty: number
  category: string
  sweet: number
  productId?: number
  product?: Product
}

export { type ReceiptItem, defaultReceiptItem }
