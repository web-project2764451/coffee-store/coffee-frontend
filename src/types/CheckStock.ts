import type { CheckStockItem } from './CheckStockItem'
import type { User } from './User'

type CheckStock = {
  id: number
  checkStockDate: Date
  totalUse: number
  employeeId: number
  employeeName: string
  employee?: User
  checkStockItems?: CheckStockItem[]
}

export { type CheckStock }
