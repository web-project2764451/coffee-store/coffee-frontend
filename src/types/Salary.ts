type Salary = {
  id?: number
  employeeId: number
  employeeName: string
  payDate: Date
  bankAcc: string
  baseSalary: number
  increaseSalary: number
  salaryReduc: number
  netSalary: number
  status: string
}
export { type Salary }
