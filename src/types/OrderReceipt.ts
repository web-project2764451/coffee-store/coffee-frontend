import type { OrderReceiptItem } from './OrderReceiptItem'
import type { User } from './User'

type OrderReceipt = {
  id: number
  receiptDate: Date
  totalQty: number
  totalPrice: number
  employeeId: number
  employeeName: string
  employee?: User
  orderReceiptItem?: OrderReceiptItem[]
}
export type { OrderReceipt }
