export type PromotionItemType = {
  id?: number
  name: string
  condition: string
  discount: number
  discountType: string
  start: string
  end: string
  status: boolean
}
