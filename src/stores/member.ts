import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'
import { useMessageStore } from './message'
import { useReceiptStore } from './receipt'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const receiptStore = useReceiptStore()
  const tel = ref('')
  const currentMember = ref<Member | null>(null)
  const members = ref<Member[]>([])
  const initialMember: Member = {
    name: '',
    tel: ''
  }
  const editedMember = ref<Member>(initialMember)

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    editedMember.value = res.data
    loadingStore.finish()
  }
  async function getMembers() {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMembers()
      members.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function getMemberByTel(tel: string) {
    loadingStore.doLoad()
    const res = await memberService.getMemberBytel(tel)
    members.value = res.data
    currentMember.value = res.data
    console.log(currentMember.value)
    receiptStore.receipt!.member = res.data
    receiptStore.calReceipt()
    loadingStore.finish()
  }
  function getCurrentMember(): Member | null {
    const strMember = localStorage.getItem('member')
    if (strMember === null) return null
    return JSON.parse(strMember)
  }

  async function saveMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      if (!member.id) {
        // Add new
        console.log('Post ' + JSON.stringify(member))
        const res = await memberService.addMember(member)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(member))
        const res = await memberService.updateMember(member)
      }
      await getMembers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.delMember(member)

    await getMembers()
    loadingStore.finish()
  }

  function clear() {
    currentMember.value = null
    tel.value = ''
    editedMember.value = JSON.parse(JSON.stringify(initialMember))
  }
  function clearSearch() {
    tel.value = ''
  }

  return {
    members,
    editedMember,
    initialMember,
    currentMember,
    tel,
    getMembers,
    saveMember,
    deleteMember,
    getMember,
    clear,
    clearSearch,
    getMemberByTel,
    getCurrentMember
  }
})
