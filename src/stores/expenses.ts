import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import type { Expenses } from "@/types/Expenses";
import { ref } from "vue";
import { useMessageStore } from "./message";
import ExpensesService from '@/services/expenses'

export const useExpensesStore = defineStore('expenses', () => {
    const loadingStore = useLoadingStore()
    const messageStore = useMessageStore()
    const expensess = ref<Expenses[]>([])
    const initialExpenses: Expenses = {
        branchId: 0,
        branchName: '',
        billType: '',
        totalAmount: 0
    }

    const editedExpenses = ref<Expenses>(JSON.parse(JSON.stringify(initialExpenses)))

    async function getExpenses(id: number) {
        try {
            loadingStore.doLoad()
            const res = await ExpensesService.getExpenses(id)
            editedExpenses.value = res.data
            loadingStore.finish()
        } catch {
            loadingStore.finish()
        }
    }

    async function getExpensess() {
        try {
            loadingStore.doLoad()
            const res = await ExpensesService.getExpensess()
            expensess.value = res.data
            loadingStore.finish()
        } catch {
            loadingStore.finish()
        }
    }

    async function saveExpenses() {
        try {
          loadingStore.doLoad()
          const expenses = editedExpenses.value
          if (!expenses.id) {
            // console.log('Post ' + JSON.stringify(stock))
            const res = await ExpensesService.addExpenses(expenses)
          } else {
            // console.log('Patch ' + JSON.stringify(stock))
            const res = await ExpensesService.updateExpenses(expenses)
          }
          await getExpensess()
          loadingStore.finish()
        } catch (e: any) {
          messageStore.showMessage(e.message)
          loadingStore.finish()
        }
    }
    
    async function deleteExpenses() {
        loadingStore.doLoad()
        const expenses = editedExpenses.value
        const res = await ExpensesService.delExpenses(expenses)
        console.log(res)
        await getExpensess()
        loadingStore.finish()
    }
    
    function clearForm() {
        editedExpenses.value = JSON.parse(JSON.stringify(initialExpenses))
    }
    
    return { expensess, editedExpenses, getExpenses, getExpensess, saveExpenses, deleteExpenses, clearForm }
})