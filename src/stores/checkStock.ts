import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import type { CheckStock } from '@/types/CheckStock'
import { ref, watch } from 'vue'
import { useAuthStore } from './auth'
import checkStockService from '@/services/checkStock'
import type { CheckStockItem } from '@/types/CheckStockItem'
import type { Stock } from '@/types/Stock'
import { useMessageStore } from './message'

export const useCheckStockStore = defineStore('checkStock', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const checkStockItems = ref<CheckStockItem[]>([])
  const checkStocks = ref<CheckStock>()
  const checkStocksData = ref<CheckStock[]>([])
  const initialCheckStockItems: CheckStockItem = {
    id: 0,
    name: '',
    lastQty: 0,
    use: 0,
    remain: 0,
    stockId: -1,
    stock: null!
  }
  const initialCheckStock: CheckStock = {
    id: 0,
    checkStockDate: new Date(),
    totalUse: 0,
    employeeId: 0,
    employeeName: '',
    employee: null!,
    checkStockItems: []!
  }
  initCheckStock()
  function initCheckStock() {
    checkStocks.value = {
      id: -1,
      checkStockDate: new Date(),
      totalUse: 0,
      employeeId: authStore.getCurrentUser()!.id!,
      employeeName: authStore.getCurrentUser()!.fullName,
      employee: authStore.getCurrentUser()!
    }
    checkStockItems.value = []
  }

  const editedCheckStockItem = ref<CheckStockItem>(
    JSON.parse(JSON.stringify(initialCheckStockItems))
  )
  const editedCheckStock = ref<CheckStock>(JSON.parse(JSON.stringify(initialCheckStock)))

  async function getCheckStock(id: number) {
    try {
      loadingStore.doLoad()
      const res = await checkStockService.getCheckStock(id)
      editedCheckStock.value = res.data[0]
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }

  async function getCheckStocks() {
    try {
      loadingStore.doLoad()
      const res = await checkStockService.getCheckStocks()
      //   console.log(res)
      checkStocks.value = res.data
      checkStocksData.value = res.data
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }

  async function saveCheckStock() {
    try {
      loadingStore.doLoad()
      await checkStockService.addCheckStock(checkStocks.value!, checkStockItems.value)
      initCheckStock()
      getCheckStocks()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  watch(
    checkStockItems,
    () => {
      calculate()
    },
    { deep: true }
  )
  async function addCheckStockItems(s: Stock) {
    const newCheckStock: CheckStockItem = {
      id: -1,
      name: s.name,
      lastQty: s.qty,
      remain: editedCheckStockItem.value.remain,
      use: s.qty - editedCheckStockItem.value.remain,
      stockId: s.id!,
      stock: s
    }
    if (newCheckStock.use < 0) return
    for (const item of checkStockItems.value) {
      if (item.name.toUpperCase() === newCheckStock.name.toUpperCase()) return
    }
    checkStockItems.value.push(newCheckStock)
    clearCheckStockItems()
  }

  const calculate = function () {
    checkStocks.value!.totalUse = 0
    for (const item of checkStockItems.value) {
      checkStocks.value!.totalUse += item.use
    }
  }

  function clearCheckStock() {
    checkStockItems.value = []
    checkStocks.value = {
      id: -1,
      checkStockDate: new Date(),
      totalUse: 0,
      employeeId: authStore.getCurrentUser()!.id!,
      employeeName: authStore.getCurrentUser()!.fullName,
      employee: authStore.getCurrentUser()!
    }
  }

  function clearCheckStockItems() {
    editedCheckStockItem.value.id = -1
    editedCheckStockItem.value.name = ''
    editedCheckStockItem.value.lastQty = 0
    editedCheckStockItem.value.remain = 0
    editedCheckStockItem.value.use = 0
  }

  async function removeCheckStockItems(checkStockItem: CheckStockItem) {
    const index = checkStockItems.value.findIndex((item) => item === checkStockItem)
    checkStockItems.value.splice(index, 1)
  }
  return {
    checkStockItems,
    checkStocks,
    checkStocksData,
    editedCheckStock,
    editedCheckStockItem,
    getCheckStock,
    getCheckStocks,
    removeCheckStockItems,
    clearCheckStock,
    clearCheckStockItems,
    addCheckStockItems,
    saveCheckStock
  }
})
