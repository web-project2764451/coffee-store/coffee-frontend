import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import type { Branch } from "@/types/Branch";
import { ref } from "vue";
import { useMessageStore } from "./message";
import branchService from '@/services/branch'

export const useBranchStore = defineStore('branch', () => {
    const loadingStore = useLoadingStore()
    const messageStore = useMessageStore()
    const branchs = ref<Branch[]>([])
    const initialBranch: Branch = {
        branchName: '',
        address: ''
    }

    const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

    async function getBranch(id: number) {
            loadingStore.doLoad()
            const res = await branchService.getBranch(id)
            editedBranch.value = res.data
            loadingStore.finish()
    }

    async function getBranchs() {
        try {
            loadingStore.doLoad()
            const res = await branchService.getBranchs()
            branchs.value = res.data
            loadingStore.finish()
        } catch {
            loadingStore.finish()
        }
    }

    async function saveBranch() {
        try {
          loadingStore.doLoad()
          const branch = editedBranch.value
          if (!branch.id) {
            // console.log('Post ' + JSON.stringify(branch))
            const res = await branchService.addBranch(branch)
          } else {
            // console.log('Patch ' + JSON.stringify(branch))
            const res = await branchService.updateBranch(branch)
          }
          await getBranchs()
          loadingStore.finish()
        } catch (e: any) {
          messageStore.showMessage(e.message)
          loadingStore.finish()
        }
    }
    
    async function deleteBranch() {
        loadingStore.doLoad()
        const branch = editedBranch.value
        const res = await branchService.delBranch(branch)
        console.log(res)
        await getBranchs()
        loadingStore.finish()
    }
    
    function clearForm() {
        editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
    }
    
    return { branchs, editedBranch, getBranch, getBranchs, saveBranch, deleteBranch, clearForm }
})