import { ref } from 'vue'
import type { PromotionItemType } from '@/types/Promotion'

export const promotionStore = () => {
  const items = ref([] as PromotionItemType[])

  const setItems = (
    data: {
      id?: number | undefined
      name: string
      condition: string
      discount: number
      discountType: string
      start: string
      end: string
      status: boolean
    }[]
  ) => {
    items.value = data // ตั้งค่ารายการโปรโมชั่นจากข้อมูลที่ได้รับมาจาก Backend
  }

  // Function to add a new promotion
  const addPromotion = (promotion: PromotionItemType) => {
    items.value.push(promotion)
  }

  // Function to delete a promotion by index
  const deletePromotion = (promotion: PromotionItemType) => {
    const index = items.value.findIndex((item) => item.id === promotion.id)
    if (index !== -1) {
      items.value.splice(index, 1)
    }
  }

  return {
    items,
    setItems, // เพิ่มฟังก์ชัน setItems เข้าไปในการ return
    addPromotion,
    deletePromotion
  }
}
