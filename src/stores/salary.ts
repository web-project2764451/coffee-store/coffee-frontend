import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import salaryService from '@/services/salary'
import type { Salary } from '@/types/Salary'
import { ref } from 'vue'
import { useMessageStore } from './message'

export const useSalaryStore = defineStore('salary', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const salarys = ref<Salary[]>([])
  const initialSalary: Salary = {
    employeeId: 0,
    employeeName: '',
    payDate: new Date(),
    bankAcc: '',
    baseSalary: 0,
    increaseSalary: 0,
    salaryReduc: 0,
    netSalary: 0,
    status: ''
  }
  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))

  function calNetSalary(item: Salary) {
    const net = item.baseSalary + item.increaseSalary - item.salaryReduc
    return net.toFixed(2)
  }

  function getFormattedDate(date: Date) {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear()
    return `${day}/${month}/${year}`
  }

  async function getSalary(id: number) {
    loadingStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadingStore.finish()
  }

  async function getSalarys() {
    try {
      loadingStore.doLoad()
      const res = await salaryService.getSalarys()
      salarys.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveSalary() {
    try {
      loadingStore.doLoad()
      const salary = editedSalary.value
      if (!salary.id) {
        const res = await salaryService.addSalary(salary)
      } else {
        console.log('Patch ' + JSON.stringify(salary))
      const res = await salaryService.updateSalary(salary)
      }

      await getSalarys()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function updatePayment() {
    try {
      loadingStore.doLoad()
      const paidSalary = editedSalary.value
      const res = await salaryService.updateSalary(paidSalary)
      await getSalarys()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function printSlip() {
    
  }

  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initialSalary))
  }

  return { salarys, editedSalary, getSalary, getSalarys, saveSalary, updatePayment,  clearForm, calNetSalary, getFormattedDate }
})
