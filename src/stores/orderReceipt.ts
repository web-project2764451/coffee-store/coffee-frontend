import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import type { OrderReceipt } from '@/types/OrderReceipt'
import { ref, watch } from 'vue'
import { useAuthStore } from './auth'
import orderReceiptService from '@/services/orderReceipt'
import type { OrderReceiptItem } from '@/types/OrderReceiptItem'
import type { Stock } from '@/types/Stock'
import { useMessageStore } from './message'

export const useOrderReceiptStore = defineStore('orderReceipt', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const orderReceiptItems = ref<OrderReceiptItem[]>([])
  const orderReceipts = ref<OrderReceipt>()
  const orderReceiptsData = ref<OrderReceipt[]>([])
  const initialOrderReceiptItems: OrderReceiptItem = {
    id: -1,
    name: '',
    price: 0,
    qty: 0,
    total: 0,
    stockId: -1,
    stock: null!
  }
  const initialOrderReceipts: OrderReceipt = {
    id: 0,
    receiptDate: new Date(),
    totalQty: 0,
    totalPrice: 0,
    employeeId: 0,
    employeeName: '',
    employee: null!,
    orderReceiptItem: []!
  }
  initOrderReceipt()
  function initOrderReceipt() {
    orderReceipts.value = {
      id: -1,
      receiptDate: new Date(),
      totalQty: 0,
      totalPrice: 0,
      employeeId: authStore.getCurrentUser()!.id!,
      employeeName: authStore.getCurrentUser()!.fullName,
      employee: authStore.getCurrentUser()!
    }
    orderReceiptItems.value = []
  }

  const editedOrderReceiptItem = ref<OrderReceiptItem>(
    JSON.parse(JSON.stringify(initialOrderReceiptItems))
  )
  const editedOrderReceipt = ref<OrderReceipt>(JSON.parse(JSON.stringify(initialOrderReceipts)))

  async function getOrderReceipt(id: number) {
    try {
      loadingStore.doLoad()
      const res = await orderReceiptService.getOrderReceipt(id)
      editedOrderReceipt.value = res.data[0]
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }

  async function getOrderReceipts() {
    try {
      loadingStore.doLoad()
      const res = await orderReceiptService.getOrderReceipts()
      //   console.log(res)
      orderReceipts.value = res.data
      orderReceiptsData.value = res.data
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }

  async function saveOrderReceipt() {
    try {
      loadingStore.doLoad()
      await orderReceiptService.addOrderReceipt(orderReceipts.value!, orderReceiptItems.value)
      initOrderReceipt()
      getOrderReceipts()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  watch(
    orderReceiptItems,
    () => {
      calculate()
    },
    { deep: true }
  )
  async function addReceiptItems(s: Stock) {
    const newOrderReceipt: OrderReceiptItem = {
      id: -1,
      name: s.name,
      price: editedOrderReceiptItem.value.price,
      qty: editedOrderReceiptItem.value.qty,
      total: editedOrderReceiptItem.value.price * editedOrderReceiptItem.value.qty,
      stockId: s.id!,
      stock: s
    }
    if (newOrderReceipt.qty <= 0 || newOrderReceipt.price <= 0) return
    for (const item of orderReceiptItems.value) {
      if (item.name.toUpperCase() === newOrderReceipt.name.toUpperCase()) return
    }
    orderReceiptItems.value.push(newOrderReceipt)
    clearReceiptItems()
  }

  const calculate = function () {
    orderReceipts.value!.totalQty = 0
    orderReceipts.value!.totalPrice = 0
    for (const item of orderReceiptItems.value) {
      orderReceipts.value!.totalPrice += item.total
      orderReceipts.value!.totalQty += item.qty
    }
  }

  function clearOrderReceipt() {
    orderReceiptItems.value = []
    orderReceipts.value = {
      id: -1,
      receiptDate: new Date(),
      totalQty: 0,
      totalPrice: 0,
      employeeId: authStore.getCurrentUser()!.id!,
      employeeName: authStore.getCurrentUser()!.fullName,
      employee: authStore.getCurrentUser()!
    }
  }

  function clearReceiptItems() {
    editedOrderReceiptItem.value.id = -1
    editedOrderReceiptItem.value.name = ''
    editedOrderReceiptItem.value.price = 0
    editedOrderReceiptItem.value.qty = 0
    editedOrderReceiptItem.value.total = 0
  }

  async function removeOrderReceiptItems(orderReceiptItem: OrderReceiptItem) {
    const index = orderReceiptItems.value.findIndex((item) => item === orderReceiptItem)
    orderReceiptItems.value.splice(index, 1)
  }
  return {
    orderReceiptItems,
    orderReceipts,
    orderReceiptsData,
    editedOrderReceipt,
    editedOrderReceiptItem,
    getOrderReceipt,
    getOrderReceipts,
    removeOrderReceiptItems,
    clearOrderReceipt,
    clearReceiptItems,
    addReceiptItems,
    saveOrderReceipt
  }
})
