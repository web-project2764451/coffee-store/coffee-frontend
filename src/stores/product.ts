import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import productService from '@/services/product'
import type { Product } from '@/types/Product'
import { useMessageStore } from './message'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const products = ref<Product[]>([])
  const product = ref<Product>()
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 2, name: 'bekery' },
    category: '',
    sweet: 100,
    image: 'noimage.png',
    files: []
  }
  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))
  initProduct()

  function initProduct() {
    product.value = {
      name: '',
      price: 0,
      type: { id: 2, name: 'bekery' },
      category: '',
      sweet: 100,
      image: 'noimage.png'
    }
  }
  async function getProduct(id: number) {
    try {
      loadingStore.doLoad()
      const res = await productService.getProduct(id)
      editedProduct.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function getProducts() {
    try {
      loadingStore.doLoad()
      const res = await productService.getProducts()
      products.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveProduct() {
    try {
      loadingStore.doLoad()
      const product = editedProduct.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product)
      }

      await getProducts()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)

    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }

  function addProduct(p: Product) {
    product.value = p
    console.log('addProduct' + p)
  }

  function updateProduct(p: Product) {
    product.value = p
    console.log('updateProduct' + product.value)
  }

  function changCategory(category: number, product: Product) {
    if (category === 1) {
      product.category = 'hot'
      updateProduct(product)
    } else if (category === 2) {
      product.category = 'cool'
      updateProduct(product)
    } else if (category === 3) {
      product.category = 'blended'
      updateProduct(product)
    }
  }

  function incSweetProduct(product: Product) {
    if (product.sweet < 200) {
      product.sweet += 50
    } else {
      product.sweet = 200
    }
    updateProduct(product)
  }

  function decSweetProduct(product: Product) {
    if (product.sweet >= 25) {
      product.sweet -= 50
    } else {
      product.sweet = 0
    }
    updateProduct(product)
  }
  function clear() {
    product.value = {
      name: '',
      price: 0,
      type: { id: 2, name: 'drink' },
      category: '',
      sweet: 100,
      image: 'noimage.png'
    }
  }

  return {
    product,
    products,
    editedProduct,
    changCategory,
    incSweetProduct,
    decSweetProduct,
    getProducts,
    saveProduct,
    deleteProduct,
    addProduct,
    getProduct,
    clearForm,
    clear,
    initProduct
  }
})
