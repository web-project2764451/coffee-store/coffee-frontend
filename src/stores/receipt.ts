import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import orderService from '@/services/order'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { useMemberStore } from './member'
import type { Product } from '@/types/Product'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const receiptItems = ref<ReceiptItem[]>([])
  const Orders = ref<Receipt[]>([])
  const receipt = ref<Receipt>()
  const receiptDialog = ref(false)
  const receiptPaperDialog = ref(false)
  const receiptPaperHistoryDialog = ref(false)
  const paymentType = ref<string>('')
  const Order = ref<Receipt>({
    id: 1,
    created: new Date(),
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    change: 0,
    paymentType: '',
    amount: 0,
    qty: 0,
    user: authStore.getCurrentUser()!,
    userId: authStore.getCurrentUser()!.id!,
    member: memberStore.initialMember!,
    memberId: 0,
    receiptItems: []
  })
  initialReceipt()

  function initialReceipt() {
    receipt.value = {
      id: 1,
      created: new Date(),
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      change: 0,
      paymentType: '',
      amount: 0,
      qty: 0,
      user: authStore.getCurrentUser()!,
      userId: authStore.getCurrentUser()!.id!,
      member: memberStore.initialMember!,
      memberId: 0
    }
    receiptItems.value = []
  }

  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )

  async function getOrder(id: number) {
    try {
      loadingStore.doLoad()
      // const res = await orderService.getOrder(id)
      // Order.value = res.data

      const selectedReceipt = Orders.value.find((receipt) => receipt.id === id)

      console.log('Before')
      console.log(selectedReceipt)
      if (selectedReceipt) {
        // ดึงข้อมูล receiptItems จากตารางหรือสถานที่เก็บข้อมูลอื่น ๆ ที่เชื่อมโยงกับ selectedReceip
        // const receiptItems = await orderService.getReceiptItems(selectedReceipt.id)
        console.log('receiptItems')
        console.log(receiptItems)
        const listReceipt: Receipt = {
          id: selectedReceipt.id,
          created: selectedReceipt.created,
          totalBefore: selectedReceipt.totalBefore,
          memberDiscount: selectedReceipt.memberDiscount,
          total: selectedReceipt.total,
          change: selectedReceipt.change,
          paymentType: selectedReceipt.paymentType,
          amount: selectedReceipt.amount,
          qty: selectedReceipt.qty,
          user: selectedReceipt.user,
          userId: selectedReceipt.userId,
          member: selectedReceipt.member,
          memberId: selectedReceipt.memberId,
          receiptItems: selectedReceipt.receiptItems || []
        }
        Order.value = listReceipt
        receiptPaperHistoryDialog.value = true
        console.log('After')
        console.log(Order.value)
      }
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function getOrders() {
    try {
      loadingStore.doLoad()
      const res = await orderService.getOrders()
      Orders.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  const calReceipt = function () {
    receipt.value!.total = 0
    receipt.value!.qty = 0
    receipt.value!.totalBefore = 0
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.totalBefore += receiptItems.value[i].price * receiptItems.value[i].qty
      receipt.value!.qty += receiptItems.value[i].qty
    }
    if (memberStore.currentMember) {
      receipt.value!.total = receipt.value!.totalBefore * 0.95
      receipt.value!.memberDiscount = receipt.value!.totalBefore * 0.05
    } else {
      receipt.value!.total = receipt.value!.totalBefore
    }
  }

  function calculateChange(): number {
    const receivedAmount = receipt.value!.amount || 0
    const total = receipt.value!.total || 0
    const change = receivedAmount - total
    receipt.value!.change = change
    return change
  }

  async function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex(
      (item) =>
        item.productId === product.id &&
        item.sweet === product.sweet &&
        item.category === product.category
    )
    if (index >= 0) {
      receiptItems.value[index].qty++
    } else {
      let price = product.price
      if (product.category === 'cool') {
        price += 5.0
      } else if (product.category === 'blended') {
        price += 10.0
      }
      const newReceiptItem: ReceiptItem = {
        id: -1,
        name: product.name,
        price: price,
        qty: 1,
        category: product.category,
        sweet: product.sweet,
        productId: product.id
      }

      await receiptItems.value.push(newReceiptItem)
      console.log(receiptItems.value)
    }
    calReceipt()
  }

  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }

  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.qty++
  }

  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.qty--
    if (selectedItem.qty === 0) {
      deleteReceiptItem(selectedItem)
    }
  }

  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  const order = async () => {
    try {
      loadingStore.doLoad()
      receipt.value!.memberId = receipt.value?.member?.id
      console.log(receipt.value)
      console.log(receiptItems.value)
      await orderService.addOrder(receipt.value!, receiptItems.value)
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function showReceiptDialog() {
    receipt.value!.receiptItems = receiptItems.value
    receiptDialog.value = true
  }

  const showReceipt = function () {
    if (receiptItems.value.length > 0) {
      receipt.value!.created = new Date()
      showReceiptDialog()
      receipt.value!.amount = 0
    } else {
      alert('กรุณาเลือกสินค้าก่อน')
    }
  }

  function setPaymentType(type: string) {
    paymentType.value = type
  }

  async function hidePaymentTypeDialog() {
    const receivedAmount = receipt.value?.amount || 0
    const total = receipt.value?.total || 0

    const change = receivedAmount - total

    if (paymentType.value === 'cash') {
      if (change > 0) {
        receipt.value!.paymentType = 'cash'
        receiptDialog.value = false
        receiptPaperDialog.value = true
        order()
      } else {
        alert('กรุณาใส่จำนวนเงินให้ถูกต้อง')
      }
    } else if (paymentType.value === 'qrcode') {
      receipt.value!.paymentType = 'qrcode'
      receipt.value!.amount = 0
      receipt.value!.change = 0
      receiptDialog.value = false
      receiptPaperDialog.value = true
      order()
    } else {
      alert('กรุณาเลือกช่องทางการชำระเงิน')
    }
  }

  async function clear() {
    console.log('clear')
    await memberStore.clear()
    await memberStore.clearSearch()
    receipt.value!.id = 0
    ;(receipt.value!.totalBefore = 0),
      (receipt.value!.memberDiscount = 0),
      (receipt.value!.total = 0),
      (receipt.value!.amount = 0),
      (receipt.value!.change = 0),
      (receipt.value!.paymentType = ''),
      (receipt.value!.memberId = 0),
      (receipt.value!.member = memberStore.initialMember),
      (receipt.value!.user = authStore.getCurrentUser()!)
    receiptItems.value = []
    console.log(receipt.value)
  }

  return {
    receipt,
    receiptItems,
    receiptDialog,
    receiptPaperDialog,
    receiptPaperHistoryDialog,
    paymentType,
    Orders,
    Order,
    getOrder,
    getOrders,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    clear,
    order,
    calReceipt,
    showReceipt,
    setPaymentType,
    calculateChange,
    hidePaymentTypeDialog
  }
})
