import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import type { Stock } from '@/types/Stock'
import { useMessageStore } from './message'
import { ref } from 'vue'
import stockService from '@/services/stock'

export const useStockStore = defineStore('stock', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const stocks = ref<Stock[]>([])
  const initialStock: Stock = {
    name: '',
    qty: 0,
    priceUnit: 0,
    value: 0
  }
  const editedStock = ref<Stock>(JSON.parse(JSON.stringify(initialStock)))

  async function getStock(id: number) {
    try {
      loadingStore.doLoad()
      const res = await stockService.getStock(id)
      editedStock.value = res.data
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }
  async function getStocks() {
    try {
      loadingStore.doLoad()
      const res = await stockService.getStocks()
      //   console.log(res)
      stocks.value = res.data
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }

  async function saveStock() {
    try {
      loadingStore.doLoad()
      const stock = editedStock.value
      if (!stock.id) {
        // console.log('Post ' + JSON.stringify(stock))
        const res = await stockService.addStock(stock)
      } else {
        // console.log('Patch ' + JSON.stringify(stock))
        const res = await stockService.updateStock(stock)
      }
      await getStocks()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function deleteStock() {
    loadingStore.doLoad()
    const stock = editedStock.value
    const res = await stockService.delStock(stock)
    console.log(res)
    await getStocks()
    loadingStore.finish()
  }

  function clearForm() {
    editedStock.value = JSON.parse(JSON.stringify(initialStock))
  }
  return { stocks, editedStock, getStock, getStocks, saveStock, deleteStock, clearForm }
})
